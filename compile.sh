#!/bin/bash
if [ -d compiled ]
then
    rm -r compiled
fi

mkdir compiled
touch "compiled/main.bin"
nasm -fbin main.asm -o "compiled/main.bin"
truncate -s 1200k "compiled/main.bin"
mkisofs -o "compiled/main.iso" -b "compiled/main.bin" ./